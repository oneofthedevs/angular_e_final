# FinalAssessment 🐱‍💻
### Final assessment for Angular Training 

### To Start the project 
#### Prerequisites

- NodeJS
- Angular CLI 

### Steps
###### Install node_modules
``` Terminal
npm i 
```


###### Start/Serve Angular App 
```
ng serve -o
```

Angular project will start running on localhost:4200
